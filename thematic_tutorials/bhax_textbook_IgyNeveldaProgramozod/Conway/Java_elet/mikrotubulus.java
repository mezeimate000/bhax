/*
 * Mikrotubulus.java
 *
 * DIGIT 2005, Javat tanítok
 * Bátfai Norbert, nbatfai@inf.unideb.hu
 *
 */
/**
 * Orch OR sejtautomata demonstráció.
 * A jelen szimulációs vagy pontosabban csupán - az [ORCH OR] Stuart
 * Hameroff és Roger Penrose, Orchestrated Objective Reduction of
 * Quantum Coherence in Brain Microtubules: The “Orch OR” Model for
 * Consciousmess,
 * http://www.quantumconsciousness.org/penrose-hameroff/orchOR.html
 * [ORCH OR TUDAT] Stuart Hameroff és Roger Penrose, Conscious Events
 * as Orchestrated Space-Time Selections,
 * http://www.quantumconsciousness.org/penrosehameroff/consciousevents.html
 * cikkek képei alapján készített - demonstrációs osztály azt mutatja,
 * hogy egyre több tubulin dimer kerül koherens állapotba, míg az
 * objektív redukció következtében egy előre nem kiszámítható módon
 * ugranak a résztvevő cellák valamely lehetséges állapotukba,
 *
 * @author Bátfai Norbert, nbatfai@inf.unideb.hu
 * @version 0.0.1
 */
public class Mikrotubulus extends java.awt.Frame implements Runnable {
    /** Rácspont az egyik állapotában. */
    public static final int EGYIK = 0;
    /** Rácspont a másik állapotában. */
    public static final int MÁSIK = 1;
    /** Rácspont az összefonódott állapotban. */
    public static final int SZUPER = 2;
    /** Hány összefonódott rácspont indítja be az OR-t? */
    public static final int EGY_GRAVITON_SZINTNYI = 235;
    /** Két ráccsal dolgozunk: a diszkrét időskála adott
     * pillanata előtti és utáni rácsot írjuk le velük. */
    int [][][] hexagonálisRácsok = new int [2][][];
    /** Melyik az éppen aktuális rács? */
    int [][] hexagonálisRács;
    /** A 0. vagy az 1. ? */
    int rácsIndex = 0;
    /* A kirajzolás adatai: */
    int cellaSzélesség = 20;
    int cellaMagasság = 20;
    /* A rács adatai: */
    int szélesség = 13;
    int magasság = 20;
    // Technikai:
    java.util.Random random = new java.util.Random();
    /* Képek a kirajzoláshoz */
    java.awt.Image fehérKép;
    java.awt.Image feketeKép;
    java.awt.Image pirosKép;
    // A pillanatfelvételekhez, technikai:
    java.awt.Robot robot;
    /**
     * Az adott méretű {@code Mikrotubulus} objektumot
     * felépítő konstruktor.
     *
     * @param   szélesség  tubulin-dimerek száma vízszintesen
     * @param   magasság  tubulin-dimerek száma függőlegesen
     */
    public Mikrotubulus(int szélesség, int magasság) {
        
        this.szélesség = szélesség;
        this.magasság = magasság;
        // A diszkrét időskála adott
        // pillanata előtti és utáni rácsok:
        hexagonálisRácsok[0] = new int[magasság][szélesség];
        hexagonálisRácsok[1] = new int[magasság][szélesség];
        
        hexagonálisRács = hexagonálisRácsok[rácsIndex];
        // a rácspontok egy véletlen állapotából indul a sejtautomata
        for(int i=0; i<hexagonálisRács.length; ++i)
            for(int j=0; j<hexagonálisRács[0].length; ++j)
                hexagonálisRács[i][j] = random.nextInt(2);
        // innen indul ki egy összefonódás
        hexagonálisRács[10][6] = SZUPER;
        
        // a program ablaka, amit be is lehet csukni
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                setVisible(false);
                System.exit(0);
            }
        });
        // Képek betöltése
        fehérKép = new javax.swing.ImageIcon
                ("fehér.png").getImage();
        feketeKép = new javax.swing.ImageIcon
                ("fekete.png").getImage();
        pirosKép = new javax.swing.ImageIcon
                ("piros.png").getImage();
        
        cellaSzélesség = fehérKép.getWidth(this);
        cellaMagasság = fehérKép.getHeight(this);
        
        // a lokális grafikus környezet elkérése
        java.awt.GraphicsEnvironment graphicsEnvironment
                = java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment();
        // a grafikus környzetből a képernyővel dolgozunk
        java.awt.GraphicsDevice graphicsDevice
                = graphicsEnvironment.getDefaultScreenDevice();
        try {
            robot = new java.awt.Robot(graphicsDevice);
        } catch(java.awt.AWTException e) {
            e.printStackTrace();
        }
        
        // Ablak jellemzőinek beállítása
        setTitle("Mikrotubulus");
        setResizable(false);
        //setUndecorated(true);
        
        setSize(szélesség*cellaSzélesség,
                magasság*cellaMagasság);
        setVisible(true);
        
        // Szimulációs (demonstrációs) szál elkészítése és beindítása
        new Thread(this).start();
    }
    /* A felület kirajzolása */
    public void paint(java.awt.Graphics g) {
        // Éppen melyik rácsot kell rajzolnunk:
        int [][] hexagonálisRács = hexagonálisRácsok[rácsIndex];
        // "Töröljük" a felületet (ha villog, töröljünk a
        // következő ciklusban kis darabokat)
        g.setColor(java.awt.Color.GRAY);
        g.fillRect(0,0, szélesség*cellaSzélesség,
                magasság*cellaMagasság);
        // Végigmegyünk a rácspontokon:
        for(int i=0; i<hexagonálisRács.length; ++i) { // sorok
            for(int j=0; j<hexagonálisRács[0].length; ++j) { // oszlopok
                
               /* Csak a rácsot rajzolja
                
                g.setColor(java.awt.Color.BLACK);
                g.drawRect(j*20, i*20+(j%2)*10, 20, 20);
                
                 Csak a rácsot rajzolja */
                
                /* Nem képeket, színes téglalapokat rajzol
                 
                if(hexagonálisRács[i][j] == EGYIK)
                    g.setColor(java.awt.Color.BLACK);
                else if(hexagonálisRács[i][j] == MÁSIK)
                    g.setColor(java.awt.Color.WHITE);
                else
                    g.setColor(java.awt.Color.RED);
                 
                // a +(j%2)*10 a páratlanadik oszlopokat
                // lefelé tolja
                g.fillRect(j*20, i*20+(j%2)*10, 20, 20);
                 
                Nem képeket, színes téglalapokat rajzol */
                
                // A szimulációs képeket rajzolja
                
                if(hexagonálisRács[i][j] == EGYIK)
                    g.drawImage(fehérKép, j*fehérKép.getWidth(this),
                            i*fehérKép.getHeight(this)
                            +(j%2)*(fehérKép.getHeight(this)/2), null);
                else if(hexagonálisRács[i][j] == MÁSIK)
                    g.drawImage(feketeKép, j*feketeKép.getWidth(this),
                            i*feketeKép.getHeight(this)
                            +(j%2)*(fehérKép.getHeight(this)/2), null);
                else
                    g.drawImage(pirosKép, j*pirosKép.getWidth(this),
                            i*pirosKép.getHeight(this)
                            +(j%2)*(fehérKép.getHeight(this)/2), null);
                
                // A szimulációs képeket rajzolja
            }
        }
        
        boolean pillanatfelvetel = false;
        if(pillanatfelvetel) {
            pillanatfelvetel = false;
            pillanatfelvetel(robot.createScreenCapture
                    (new java.awt.Rectangle
                    (0, 0, szélesség*cellaSzélesség,
                    magasság*cellaMagasság)));
        }
    }
    /* Technikai a villogás ellen (nem meszeli le a hátteret az update(),
     mert most felüldefiniáljuk meszelés nélkül). */
    public void update(java.awt.Graphics g) {
        paint(g);
    }
    // A pillanatfelvételek számozásához
    public static int fotoSzamlalo = 0;
    /** ScreenShot készítése. */
    public void pillanatfelvetel(java.awt.image.BufferedImage felvetel) {
        // tech. a sztringezéshez: a képfájl nevének előállítása
        StringBuffer sb = new StringBuffer();
        sb = sb.delete(0, sb.length());
        sb.append("mikrotubulus");
        sb.append(++fotoSzamlalo);
        sb.append(".png");
        // a kép mentése
        try {
            // png-t mentünk
            javax.imageio.ImageIO.write(felvetel, "png",
                    new java.io.File(sb.toString()));
        } catch(java.io.IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * A rács adott cellájának hány szomszéda van a kérdezett állapotban?
     */
    public int szomszédokSzáma(int [][] hexagonálisRács,
            int sor, int oszlop, int állapot) {
        
        int állapotúSzomszéd = 0;
        
        if(oszlop%2 == 1) {
            
            if(sor-1>0)
                if(hexagonálisRács[sor-1][oszlop] == állapot)
                    ++állapotúSzomszéd;
            if(sor+1<magasság)
                if(hexagonálisRács[sor+1][oszlop] == állapot)
                    ++állapotúSzomszéd;
            
            if(oszlop-1>0) {
                if(hexagonálisRács[sor][oszlop-1] == állapot)
                    ++állapotúSzomszéd;
                if(sor+1<magasság)
                    if(hexagonálisRács[sor+1][oszlop-1] == állapot)
                        ++állapotúSzomszéd;
            }
            
            if(oszlop+1<szélesség) {
                if(hexagonálisRács[sor][oszlop+1] == állapot)
                    ++állapotúSzomszéd;
                if(sor+1<magasság)
                    if(hexagonálisRács[sor+1][oszlop+1] == állapot)
                        ++állapotúSzomszéd;
            }
            
        } else {
            
            if(sor-1>0)
                if(hexagonálisRács[sor-1][oszlop] == állapot)
                    ++állapotúSzomszéd;
            if(sor+1<magasság)
                if(hexagonálisRács[sor+1][oszlop] == állapot)
                    ++állapotúSzomszéd;
            
            if(oszlop-1>0) {
                if(hexagonálisRács[sor][oszlop-1] == állapot)
                    ++állapotúSzomszéd;
                if(sor-1>0)
                    if(hexagonálisRács[sor-1][oszlop-1] == állapot)
                        ++állapotúSzomszéd;
            }
            
            if(oszlop+1<szélesség) {
                if(hexagonálisRács[sor][oszlop+1] == állapot)
                    ++állapotúSzomszéd;
                if(sor-1>0)
                    if(hexagonálisRács[sor-1][oszlop+1] == állapot)
                        ++állapotúSzomszéd;
            }
            
        }
        
        return állapotúSzomszéd;
    }
    /** Van összefonódott állapotban lévő szomszédja a rács
     * adott cellájának? */
    public boolean vanSzuperSzomszéd(int [][] hexagonálisRács,
            int sor, int oszlop) {
        
        if(oszlop%2 == 1) {
            
            if(sor-1>0)
                if(hexagonálisRács[sor-1][oszlop] == SZUPER)
                    return true;
            if(sor+1<magasság)
                if(hexagonálisRács[sor+1][oszlop] == SZUPER)
                    return true;
            
            if(oszlop-1>0) {
                if(hexagonálisRács[sor][oszlop-1] == SZUPER)
                    return true;
                if(sor+1<magasság)
                    if(hexagonálisRács[sor+1][oszlop-1] == SZUPER)
                        return true;
            }
            
            if(oszlop+1<szélesség) {
                if(hexagonálisRács[sor][oszlop+1] == SZUPER)
                    return true;
                if(sor+1<magasság)
                    if(hexagonálisRács[sor+1][oszlop+1] == SZUPER)
                        return true;
            }
            
        } else {
            
            if(sor-1>0)
                if(hexagonálisRács[sor-1][oszlop] == SZUPER)
                    return true;
            if(sor+1<magasság)
                if(hexagonálisRács[sor+1][oszlop] == SZUPER)
                    return true;
            
            if(oszlop-1>0) {
                if(hexagonálisRács[sor][oszlop-1] == SZUPER)
                    return true;
                if(sor-1>0)
                    if(hexagonálisRács[sor-1][oszlop-1] == SZUPER)
                        return true;
            }
            
            if(oszlop+1<szélesség) {
                if(hexagonálisRács[sor][oszlop+1] == SZUPER)
                    return true;
                if(sor-1>0)
                    if(hexagonálisRács[sor-1][oszlop+1] == SZUPER)
                        return true;
            }
            
        }
        
        return false;
    }
    /** A szimuláció időfejlődése, diszkrét időskálán dolgozunk. */
    public void időFejlődés() {
        
        /* Két ráccsal dolgozunk: a diszkrét időskála adott
           pillanata előtti és utáni ráccsal: */        
        int [][] hexagonálisRácsElőtte = hexagonálisRácsok[rácsIndex];
        int [][] hexagonálisRácsUtána = hexagonálisRácsok[(rácsIndex+1)%2];
        
        // Hány összefonódott van ebben az időpillanatban?
        int szuperszámláló = 0;
        
        for(int i=0; i<hexagonálisRácsElőtte.length; ++i) { // sorok
            for(int j=0; j<hexagonálisRácsElőtte[0].length; ++j) { // oszlopok
                
                if(hexagonálisRácsElőtte[i][j] == SZUPER) {
                    // Összefonódott az is marad
                    hexagonálisRácsUtána[i][j] = hexagonálisRácsElőtte[i][j];
                    ++szuperszámláló;
                    
                } else {
                    // Egyébként a következő, hasraütésre vett
                    // átmeneti szabályokat alkalmazzuk:
                    if(vanSzuperSzomszéd(hexagonálisRácsElőtte, i, j)) {
                        
                        if(random.nextInt(4) > 0) {
                            hexagonálisRácsUtána[i][j] = SZUPER;
                            ++szuperszámláló;
                        }
                        
                    } else if(random.nextInt(125) == 0) {
                        
                        hexagonálisRácsUtána[i][j] = SZUPER;
                        ++szuperszámláló;
                        
                    } else {
                        
                        int egyik = szomszédokSzáma(hexagonálisRácsElőtte,
                                i, j, EGYIK);
                        int másik = szomszédokSzáma(hexagonálisRácsElőtte,
                                i, j, MÁSIK);
                        
                        if(hexagonálisRácsElőtte[i][j] == EGYIK) {
                            
                            if(egyik == másik)
                                hexagonálisRácsUtána[i][j] = MÁSIK;
                            
                        }  else {
                            
                            if(másik < egyik)
                                hexagonálisRácsUtána[i][j] = EGYIK;
                            else
                                hexagonálisRácsUtána[i][j] = MÁSIK;
                            
                        }
                    }
                }
                // ha elértük az egy graviton szintet, akkor
                // bekövetkezik 
                if(szuperszámláló >= EGY_GRAVITON_SZINTNYI) {
                    // az OR folyamat:
                    állapotvektorRedukció();
                    szuperszámláló = 0;
                }
            }
        }
        // a régi rács lesz az új:
        rácsIndex = (rácsIndex+1)%2;
    }    
    /** Az OR folyamat demonstrációja. */
    public void állapotvektorRedukció() {
        
        int [][] hexagonálisRács1 = hexagonálisRácsok[rácsIndex];
        
        for(int i=0; i<hexagonálisRács.length; ++i)
            for(int j=0; j<hexagonálisRács[0].length; ++j) {
            if(hexagonálisRácsok[0][i][j] == SZUPER ||
                    hexagonálisRácsok[1][i][j] == SZUPER) {
                hexagonálisRácsok[0][i][j] = random.nextInt(2);
                hexagonálisRácsok[1][i][j] = hexagonálisRácsok[0][i][j];
            }
            }
        
        hexagonálisRácsok[0][2+random.nextInt(magasság-2)]
                [2+random.nextInt(szélesség-2)] = SZUPER;
        hexagonálisRácsok[1][2+random.nextInt(magasság-2)]
                [2+random.nextInt(szélesség-2)] = SZUPER;
        
    }
    /** A szimulációs szál időfejlődése. */
    public void run() {
        
        while(true) {
            try {
                
                Thread.sleep(500);
                
            } catch (InterruptedException e) {}
            
            időFejlődés();
            repaint();
        }
    }
    
    public static void main(String[] args) {
        new Mikrotubulus(13, 20);
    }
}	

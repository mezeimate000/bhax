package binfatest;

import java.io.FileInputStream;
import java.io.IOException;
//z3a17_from_scratch.cpp
public class Binfatest {

    public static void main(String[] args) {

        try {
            java.io.InputStream input;
            java.io.Writer output = new java.io.FileWriter(args[2]);

            input = new FileInputStream(args[0]);

            char ch;
            BinTree lzw = new BinTree();
            
            while (0< input.available()){
                ch = (char)input.read();
                if(0x0a == ch) break;
            }
            
            
            boolean inComment = false;
        while(0 < input.available())
        {
            ch = (char)input.read();
 
                    if(0x3e == ch)
                    {
                            inComment = true;
                            continue;
                    }
                    if(0x0a == ch)
            {
                            inComment = false;
                            continue;
                    }
                    if(inComment) continue;
                    if(0x4e == ch) continue;
 
                    for(int i=0; i<8; i++)
            {
                            if((ch & 0x80)!=0)
                                    lzw.insert('1');
                            else
                                    lzw.insert('0');
                            ch <<=1 ;
                    }
        }
             
             lzw.print(output);     
             output.write("depth = " + lzw.getDepth() + '\n');
             output.write("mean = " + lzw.getAvg() + '\n');
             output.write("var = " + lzw.getVar() + '\n');
 
             input.close();
             output.close();    
            

        } catch (IOException e) {
            System.out.println("Hiba: " + e);
        }

    }

}

class BinTree {

    private class Node {

        private char letter;
        private Node left;
        private Node right;
        private int count = 0;

        public Node() {
            this.letter = '\\';
        }

        public Node(char c) {
            this.letter = c;
        }

        public char getLetter() {
            return this.letter;
        }

        public Node getleftChild() {
            return left;
        }

        public Node getrightChild() {
            return right;
        }

        public void AddLeftChild(Node newLeftChild) {
            this.left = newLeftChild;
        }

        public void AddRightChild(Node newRightChild) {
            this.right = newRightChild;
        }

        public int getCount() {
            return count;
        }

        public void incCount() {
            count++;
        }

    }

    private Node currNode;
    private int depth;
    private int avgSum;
    private int avgCount;

    private double varSum;

    Node root = new Node();
    int maxDepht;
    double avg, var;

    public BinTree() {
        this.currNode = this.root;
    }

    public void insert(char a) {
        if ('0' == a) {
            if (null == currNode.getleftChild()) {
                currNode.AddLeftChild(new Node('0'));
                currNode = root;
            } else {
                currNode = currNode.getleftChild();
            }
        } else {
            if (null == currNode.getrightChild()) {
                currNode.AddRightChild(new Node('1'));
                currNode = root;
            } else {
                currNode = currNode.getrightChild();
            }

        }
    }

    public void print(java.io.Writer output) throws IOException {
        depth = 0;

        print(root, output);
    }

    public int getDepth() {
        depth = 0;
        maxDepht = 0;
        _getDepth(root);
        return maxDepht - 1;
    }

    public double getAvg() {
        depth = 0;
        avgSum = 0;
        avgCount = 0;

        _getAvg(root);
        avg = ((double) avgSum) / avgCount;
        return avg;

    }

    public double getVar() {
        avg = getAvg();
        varSum = 0.0;
        depth = 0;
        avgCount = 0;

        _getVar(root);

        if (avgCount - 1 > 0) {
            var = Math.sqrt(varSum / (avgCount - 1));
        } else {
            var = Math.sqrt(varSum);
        }

        return var;
    }

    private void print(Node node, java.io.Writer output) throws IOException {
        if (null != node) {
            depth++;
            print(node.getrightChild(), output);

            for (int i = 0; i < depth; i++) {
                output.write(new String("---"));
            }
            output.write(node.getLetter() + "(" + (depth - 1) + ")" + "\n");

            print(node.getleftChild(), output);
            depth--;
        }
    }

    protected void _getDepth(Node node) {
        if (null != node) {
            depth++;

            if (depth > maxDepht) {
                maxDepht = depth;
            }

            _getDepth(node.getrightChild());
            _getDepth(node.getleftChild());
            depth--;

        }
    }

    protected void _getAvg(Node node) {
        if (null != node) {
            ++depth;
            _getAvg(node.getrightChild());
            _getAvg(node.getleftChild());
            --depth;
            if (null == node.getrightChild() && null == node.getleftChild()) {
                avgCount++;
                avgSum += depth;
            }
        }
    }

    protected void _getVar(Node node) {
        if (null != node) {
            ++depth;
            _getVar(node.getrightChild());
            _getVar(node.getleftChild());
            --depth;
            if (null == node.getrightChild() && null == node.getleftChild()) {
                avgCount++;;
                varSum += ((depth - avg) * (depth - avg));
            }
        }
    }

}

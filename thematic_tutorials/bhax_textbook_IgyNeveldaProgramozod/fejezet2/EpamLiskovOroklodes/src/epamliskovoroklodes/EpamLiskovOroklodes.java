package epamliskovoroklodes;
/**
 *
 * @author Feeld
 */
public class EpamLiskovOroklodes {
    public static void main(String[] args) {
        
        Vehicle firstVehicle = new Supercar();
        firstVehicle.start();
        
        System.out.println(firstVehicle instanceof Car); //firstVehicle objektumpéldány egy adott osztálynak (Car-nak) tagja-e
        
        Car secondVehicle = (Car) firstVehicle;
        secondVehicle.start();
        
        System.out.println(secondVehicle instanceof Supercar);
        
        //Supercar thirdVehicle = new Vehicle();
        Supercar thirdVehicle = (Supercar) new Vehicle();
        
        //Nem fogunk tudni létrehozni szülő típusú példányt gyerek/leszármazott osztályból az öröklődés miatt
        thirdVehicle.start();
    }
}
class Vehicle{
    //Vehicle Constructor
    public Vehicle() {
        System.out.println("Vehicle");
    }
    public void start(){
        System.out.println("START");
    }
}

class Car extends Vehicle{
    //Car Constructor
    public Car() {
        System.out.println("Car");
    }
    
    //felülírt örökölt start metódus a szülő Vehicle osztályból
    @Override
    public void start() {
        super.start(); 
    }
}

class Supercar extends Car {
    //Supercar Constructor
    public Supercar() {
        System.out.println("Supercar");
    }
    
    //felülírt örökölt start metódus a szülő Vehicle osztályból
    @Override
    public void start() {
        super.start();
    }
    
    
}
package hu.fersml.aranyfc;

public class Vedo extends hu.fersml.aranyfc.Jatekos {

  /**/
  protected boolean latomASajatGolvonalat;
  protected double distanceSajatGolvonal;
  protected double directionSajatGolvonal;
  protected double distChangeSajatGolvonal;
  protected double dirChangeSajatGolvonal;
  /**/
  protected boolean latomAKozepet;
  protected double distanceKozep;
  protected double directionKozep;
  protected double distChangeKozep;
  protected double dirChangeKozep;
  /**/
  protected boolean mozoghat;

  @Override
  public void preInfo() {
    super.preInfo();
    latomAKozepet = false;
    latomASajatGolvonalat = false;
    mozoghat = false;
  }

  @Override
  public void postInfo() {

    if (latomAFocit && distanceFoci < 4.0) {
      mozoghat = true;
    }

    if (kirugas) {
      kirugashoz();
    } else if (kozepkezdes) {
      kozepkezdes();
    } else {

      jatekbanVezerles();
    }
  }

  @Override
  protected void jatekbanVezerles() {

    if (latomAKozepet && distanceKozep < 10.0) {
      sarkonFordul();
    } else if (latomASajatGolvonalat && distanceSajatGolvonal < 0.5) {
      sarkonFordul();
    } else if (latomASzelet && distanceSzele < 5.5) {
      palyanMaradni();
    } else if (latomAFocit) {
      if (distanceFoci < 0.7) {
        rugdEsFuss();
      } else if (distanceFoci < 1.4) {
        becsuszas();
      } else if (distanceFoci < 15.0) {
        egyuttElaJatekkal(100);
      } else if (distanceFoci < 25.0) {
        egyuttElaJatekkal(80);
      } else if (distanceFoci < 30.0) {
        egyuttElaJatekkal(70);
      } else if (distanceFoci < 35.0) {
        egyuttElaJatekkal(40);
      } else {
        egyuttElaJatekkal(5);
      }
    } else {
      teblabol();
    }
  }

  @Override
  public void infoSeeFlagGoalOwn(atan.model.enums.Flag flag, double distance, double direction, double distChange, double dirChange,
          double bodyFacingDirection, double headFacingDirection) {

    latomASajatGolvonalat = true;
    distanceSajatGolvonal = distance;
    directionSajatGolvonal = direction;
    dirChangeSajatGolvonal = distChange;
    dirChangeSajatGolvonal = dirChange;
    if (distance > 4.0) {
      mozoghat = true;
    }

  }

  @Override
  public void infoSeeFlagCenter(atan.model.enums.Flag flag, double distance, double direction, double distChange, double dirChange,
          double bodyFacingDirection, double headFacingDirection) {

    latomAKozepet = true;
    distanceKozep = distance;
    directionKozep = direction;
    dirChangeKozep = distChange;
    dirChangeKozep = dirChange;
    if (distance > 16.0) {
      mozoghat = true;
    }

  }

  /* 4-3-3 */
  @Override
  protected void kozepkezdes() {

    switch (getPlayer().getNumber()) {

      // Vedo

      case 8:
        getPlayer().move(-35, -19);
        break;

      case 9:
        getPlayer().move(-33, -10);
        break;

      case 10:
        getPlayer().move(-34, 12);
        break;

      case 11:
        getPlayer().move(-35, 24);
        break;

    }

  }

  @Override
  protected void egyuttElaJatekkal(int seb) {

    if (mozoghat) {
      super.egyuttElaJatekkal(seb);
    }

  }

  @Override
  protected void rugdEsFuss() {

    if (mozoghat) {
      super.rugdEsFuss();
    }
  }
}

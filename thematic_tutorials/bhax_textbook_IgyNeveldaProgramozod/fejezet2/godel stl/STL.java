import java.util.ArrayList;

public class STL {
    public static void main(String[] args) {
        ArrayList<Ertekek> adatok = new ArrayList<>();
        
        adatok.add(new Ertekek("a",3));
        adatok.add(new Ertekek("b",10));
        adatok.add(new Ertekek("c",6));
        adatok.add(new Ertekek("d",1));
        adatok.add(new Ertekek("e",99));
        adatok.add(new Ertekek("g",15));
        adatok.add(new Ertekek("h",47));
        adatok.add(new Ertekek("i",83));
        adatok.add(new Ertekek("j",2));
        
        for (int i = 0; i < adatok.size(); i++)
            System.out.println(adatok.get(i).toString());
        
        int tempVar;
        String temp;
        for (int i = 0; i < adatok.size()-1; i++){
            for(int j = 0; j < adatok.size()-i-1; j++){
                if(adatok.get(j).ertek > adatok.get(j+1).ertek){
                    tempVar = adatok.get(j+1).ertek;
                    temp = adatok.get(j+1).nev;
                    
                    adatok.get(j+1).ertek = adatok.get(j).ertek;
                    adatok.get(j+1).nev = adatok.get(j).nev;
                    
                    adatok.get(j).ertek = tempVar;
                    adatok.get(j).nev = temp;
                }
            }
        }
        System.out.println("-   -   -   -   -   -   -");
        for (int i = 0; i < adatok.size(); i++)
            System.out.println(adatok.get(i).toString());
    }
}

class Ertekek{
    String nev;
    int ertek;

    public Ertekek(String nev, int ertek) {
        this.nev = nev;
        this.ertek = ertek;
    }

    @Override
    public String toString() {
        return "Ertekek{" + "nev=" + nev + ", ertek=" + ertek + '}';
    }
}

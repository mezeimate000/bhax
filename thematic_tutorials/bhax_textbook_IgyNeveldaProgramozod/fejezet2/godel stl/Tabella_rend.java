import java.util.ArrayList;
import java.util.Collections;

public class Tabella_rend {
    public static void main(String[] args) {
        ArrayList<Tabella> adatok = new ArrayList<>();
        
        adatok.add(new Tabella("Andrea",3));
        adatok.add(new Tabella("Józsi",1));
        adatok.add(new Tabella("Júlia",16));
        adatok.add(new Tabella("Mária",9));
        adatok.add(new Tabella("Gergő",23));
        adatok.add(new Tabella("Roland",2));
        adatok.add(new Tabella("Niki",11));
        adatok.add(new Tabella("Ferenc",18));
        
        for (int i = 0; i < adatok.size(); i++)
            System.out.println(adatok.get(i).toString());
        
        Collections.sort(adatok);
        System.out.println("-   -   -   -   -   -   -   -");
        
        for (int i = 0; i < adatok.size(); i++)
            System.out.println(adatok.get(i).toString());
    }
}

class Tabella implements Comparable<Tabella>{
    String nev;
    int ertek;

    public Tabella(String nev, int ertek) {
        this.nev = nev;
        this.ertek = ertek;
    }

    @Override
    public String toString() {
        return "Tabella{" + "nev=" + nev + ", ertek=" + ertek + '}';
    }

    @Override
    public int compareTo(Tabella adatok) {
        return this.ertek - adatok.ertek;
    }   
}

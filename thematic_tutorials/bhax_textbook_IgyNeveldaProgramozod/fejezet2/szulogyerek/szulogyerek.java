public class szulogyerek{
	public static void main(String args[]){
		Szulo szulo = new Gyerek();
		szulo.szuloT();
		szulo.gyerekT();
	}
}

class Szulo{
	public void szuloT(){
		System.out.println("A szulo");
	}
}

class Gyerek extends Szulo{
	public void gyerekT(){
		System.out.println("A gyerek");
	}
}

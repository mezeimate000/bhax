
#ifndef ASZTALI_GEP_H
#define ASZTALI_GEP_H

#include vektor



/**
  * class asztali_gep
  * 
  */

class asztali_gep
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  asztali_gep();

  /**
   * Empty Destructor
   */
  virtual ~asztali_gep();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * @return bool
   */
  bool bekapcsol() override
  {
  }


  /**
   * @return bool
   */
  bool jatszani_vele() override
  {
  }


  /**
   * @return bool
   */
  bool dolgozni_vele() override
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  


  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //

};

#endif // ASZTALI_GEP_H

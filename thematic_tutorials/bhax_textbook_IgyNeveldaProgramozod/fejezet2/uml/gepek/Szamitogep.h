
#ifndef SZAMITOGEP_H
#define SZAMITOGEP_H

#include string
#include vektor



/**
  * class Szamitogep
  * 
  */

class Szamitogep
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Szamitogep();

  /**
   * Empty Destructor
   */
  virtual ~Szamitogep();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * @return bool
   */
  bool jatszani_vele()
  {
  }


  /**
   * @return bool
   */
  bool dolgozni_vele()
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  


  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  

  int ram_meret;
  int hdd_meret;

  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //  


  /**
   * Set the value of ram_meret
   * @param new_var the new value of ram_meret
   */
  void setRam_meret(int new_var)
  {
    ram_meret = new_var;
  }

  /**
   * Get the value of ram_meret
   * @return the value of ram_meret
   */
  int getRam_meret()
  {
    return ram_meret;
  }

  /**
   * Set the value of hdd_meret
   * @param new_var the new value of hdd_meret
   */
  void setHdd_meret(int new_var)
  {
    hdd_meret = new_var;
  }

  /**
   * Get the value of hdd_meret
   * @return the value of hdd_meret
   */
  int getHdd_meret()
  {
    return hdd_meret;
  }

  void initAttributes();

};

#endif // SZAMITOGEP_H


#ifndef ELEKTRONIKA_H
#define ELEKTRONIKA_H


/**
  * class Elektronika
  * 
  */

class Elektronika
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Elektronika();

  /**
   * Empty Destructor
   */
  virtual ~Elektronika();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  


  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //

};

#endif // ELEKTRONIKA_H

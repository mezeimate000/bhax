
#ifndef LAPTOP_H
#define LAPTOP_H

#include string
#include vektor



/**
  * class laptop
  * 
  */

class laptop
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  laptop();

  /**
   * Empty Destructor
   */
  virtual ~laptop();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * @return bool
   */
  bool felnyitva()
  {
  }


  /**
   * @return bool
   */
  bool bekapcsol() override
  {
  }


  /**
   * @return bool
   */
  bool jatszani_vele() override
  {
  }


  /**
   * @return bool
   */
  bool dolgozni_vele() override
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  


  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //

};

#endif // LAPTOP_H


#ifndef HAZTARTASI_H
#define HAZTARTASI_H

#include string
#include vektor



/**
  * class Haztartasi
  * 
  */

class Haztartasi
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Haztartasi();

  /**
   * Empty Destructor
   */
  virtual ~Haztartasi();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * @return bool
   */
  bool kinyit()
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  


  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  

  string szin;

  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //  


  /**
   * Set the value of szin
   * @param new_var the new value of szin
   */
  void setSzin(string new_var)
  {
    szin = new_var;
  }

  /**
   * Get the value of szin
   * @return the value of szin
   */
  string getSzin()
  {
    return szin;
  }

  void initAttributes();

};

#endif // HAZTARTASI_H

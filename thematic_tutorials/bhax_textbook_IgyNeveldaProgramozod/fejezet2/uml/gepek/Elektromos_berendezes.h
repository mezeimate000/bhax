
#ifndef ELEKTROMOS_BERENDEZES_H
#define ELEKTROMOS_BERENDEZES_H

#include vektor



/**
  * class Elektromos_berendezes
  * 
  */

class Elektromos_berendezes
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Elektromos_berendezes();

  /**
   * Empty Destructor
   */
  virtual ~Elektromos_berendezes();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * @return bool
   */
  bool bekapcsol()
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  


  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //

};

#endif // ELEKTROMOS_BERENDEZES_H
